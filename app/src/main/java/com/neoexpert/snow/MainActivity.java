package com.neoexpert.snow;

import android.app.*;
import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.net.*;
import android.os.*;
import android.view.*;
import android.view.inputmethod.*;
import android.widget.*;
import java.io.*;
import java.util.*;

public class MainActivity extends Activity implements
CompoundButton.OnCheckedChangeListener,
NumberPicker.OnValueChangeListener,
View.OnTouchListener,
View.OnLongClickListener
{

	@Override
	protected void onResume()
	{
		// TODO: Implement this method
		super.onResume();
		if(sv!=null)
			sv.resume();
	}

	@Override
	protected void onPause()
	{
		// TODO: Implement this method
		super.onPause();
		if(sv!=null)
			sv.pause();
	}

	@Override
	public boolean onLongClick(View p1)
	{

		return false;
	}


	@Override
	public boolean onTouch(View p1, MotionEvent e)
	{



		// TODO: Implement this method
		if (cp.getVisibility() == View.GONE)
			return sp.touch(e);
		else
			return false;
	}




	SnowPlot sp;

	private static final int SELECT_PHOTO = 1;
	@Override
	public void onValueChange(NumberPicker np, int p2, int value)
	{
		switch (np.getId())
		{
			case R.id.nbFPS:
				sp.setMaxFPS(value);
				break;
			case R.id.nbFlakes:
				sp.setFlakeLimit(new Integer(nums.get(value)));
				break;
		}
	}

	@Override
	protected void onStop()
	{
		sp.SaveBottom(0);
		super.onStop();
	}




	@Override
	public void onCheckedChanged(CompoundButton p1, boolean checked)
	{
		if (checked)
		{
			setEnabled(((LinearLayout)findViewById(R.id.lll)), false);
			setEnabled(((LinearLayout)findViewById(R.id.llr)), true);
			sp.setMaxFPS(-1);
			sp.setFlakeLimit(new Integer(nums.get(npFlakes.getValue())));

		}
		else
		{
			setEnabled(((LinearLayout)findViewById(R.id.lll)), true);
			setEnabled(((LinearLayout)findViewById(R.id.llr)), false);
			sp.setMaxFPS(npFPS.getValue());

		}
	}
	void setEnabled(LinearLayout ll, boolean enabled)
	{
		for (int i = 0; i < ll.getChildCount();  i++)
		{
			View view = ll.getChildAt(i);
			view.setEnabled(enabled); // Or whatever you want to do with the view.
		}
	}
	LinearLayout cp;
	Switch s;
	ArrayList<String> nums;
	NumberPicker npFPS;
	NumberPicker npFlakes;
	boolean hasNavBar(Context context) {
		Resources resources = context.getResources();
		int id = resources.getIdentifier("config_showNavigationBar", "bool", "android");
		if (id > 0) {
			return resources.getBoolean(id);
		} else {    // Check for keys
			boolean hasMenuKey = ViewConfiguration.get(context).hasPermanentMenuKey();
			boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
			return !hasMenuKey && !hasBackKey;
		}
	}
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
		
		if (BuildConfig.DEBUG)
			Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
		}
		
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
        setContentView(R.layout.main);
		
		boolean hasMenuKey = ViewConfiguration.get(this).hasPermanentMenuKey();
		boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
		if (hasMenuKey)
			findViewById(R.id.menuButton).setVisibility(View.GONE);
		else
			findViewById(R.id.menuButton).setVisibility(View.VISIBLE);
		
		if(hasNavBar(this)) 
		{
			Resources resources = this.getResources();
			int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
			if (resourceId > 0) {
				LinearLayout mb=(LinearLayout)findViewById(R.id.menuButtonLL);
				mb.setPadding(0,0,0, resources.getDimensionPixelSize(resourceId));
			}
			
			// Do whatever you need to do, this device has a navigation bar
		}
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		
		
		sv = (SnowView)findViewById(R.id.snowview);
		sp = new SnowPlot(this, size.x, size.y);
		
		sv.setOnTouchListener(this);
		sv.setOnLongClickListener(this);
		sv.setSp(sp);
		cp = (LinearLayout)findViewById(R.id.controlPanel);
		cp.setVisibility(View.GONE);
		s = (Switch)findViewById(R.id.fpsSwitch);
		s.setOnCheckedChangeListener(this);
		setEnabled(((LinearLayout)findViewById(R.id.lll)), true);
		setEnabled(((LinearLayout)findViewById(R.id.llr)), false);
		npFPS = (NumberPicker) findViewById(R.id.nbFPS);
		npFPS.setMaxValue(100);
		npFPS.setMinValue(1);
		npFPS.setValue(20);
		npFPS.setOnValueChangedListener(this);
		npFlakes = (NumberPicker) findViewById(R.id.nbFlakes);
		nums = new ArrayList<String>();
		nums.add("" + 1);
		for (int i=0; i < 10; i++)
			nums.add("" + ((i + 1) * 10));
		for (int i=1; i < 10; i++)
			nums.add("" + ((i + 1) * 100));
		for (int i=1; i < 10; i++)
			nums.add("" + ((i + 1) * 1000));
		npFlakes.setMaxValue(nums.size() - 1);
		npFlakes.setMinValue(0);
		npFlakes.setWrapSelectorWheel(false);
		String[] a=nums.toArray(new String[0]);
		npFlakes.setDisplayedValues(a);
		npFlakes.setValue(10);
		npFlakes.setOnValueChangedListener(this);
    }

	@Override
	public void onWindowFocusChanged(boolean hasFocus)
	{
		if(sp.h<sv.getHeight()){
		sp = new SnowPlot(this, sv.getWidth(), sv.getHeight());

		
		sv.setSp(sp);
		}
		super.onWindowFocusChanged(hasFocus);
	}
	
	@Override
	public void onBackPressed()
	{
		if (cp.getVisibility() == View.VISIBLE)
		{
			cp.setVisibility(View.GONE);
			if (!ViewConfiguration.get(this).hasPermanentMenuKey())
				findViewById(R.id.menuButton).setVisibility(View.VISIBLE);
		}
		else
			super.onBackPressed();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_MENU)
		{
			cp.setVisibility(View.VISIBLE);
			return true;
		}

		return super.onKeyDown(keyCode, event);
	}

	SnowView sv;
	public void onClick(View v)
	{
		switch (v.getId())
		{
			case R.id.cbCF:
				sp.setColorful(((CheckBox)v).isChecked());
				break;
			case R.id.reset:
				sp.resetSugrob();
				break;
			case R.id.snowview:
				cp.setVisibility(View.GONE);
				if (!ViewConfiguration.get(this).hasPermanentMenuKey())
					findViewById(R.id.menuButton).setVisibility(View.VISIBLE);

				break;
			case R.id.menuButton:
				cp.setVisibility(View.VISIBLE);
				findViewById(R.id.menuButton).setVisibility(View.GONE);

				break;
			case R.id.sendButton:
				EditText et=(EditText)findViewById(R.id.mainET);
				if (et.getText().length() > 0)
					sp.snowText(et.getText().toString());
				cp.setVisibility(View.GONE);
				findViewById(R.id.menuButton).setVisibility(View.VISIBLE);


				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(et.getWindowToken(), 0);

				break;
			case R.id.snowPic:
				Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
				photoPickerIntent.setType("image/*");
				startActivityForResult(photoPickerIntent, SELECT_PHOTO);
				break;
			case R.id.setWallpaper:

				Intent intent = new Intent(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);
				intent.putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT,
								new ComponentName(this, SnowWallpaperService.class));
				startActivity(intent);

				break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		switch (requestCode)
		{ 
			case SELECT_PHOTO:
				if (resultCode == RESULT_OK)
				{
					cp.setVisibility(View.GONE);
					findViewById(R.id.menuButton).setVisibility(View.VISIBLE);

					final Uri imageUri = data.getData();
					InputStream imageStream=null;
					try
					{
						imageStream = getContentResolver().openInputStream(imageUri);
					}
					catch (FileNotFoundException e)
					{
						return;
					}
					final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
					sp.drawImage(selectedImage, 0);

					// TODO: Implement this method
					super.onActivityResult(requestCode, resultCode, data);
				}

		}
	}
}
