package com.neoexpert.snow;
import android.content.*;
import android.graphics.*;
import android.preference.*;
import java.io.*;
import java.text.*;
import java.util.*;
import java.lang.annotation.*;
import android.view.*;
import android.util.*;
import java.util.concurrent.*;

public class SnowPlot
{
	double angle;
	int m[];
	//ArrayList<Flake> pl=new ArrayList<Flake>();
	List<Flake> pl = 
	Collections.synchronizedList(new ArrayList<Flake>());

	List<List<Flake>> tfl = 
	Collections.synchronizedList(new ArrayList<List<Flake>>());

	ArrayBlockingQueue<Flake> abq;
	Paint paint=new Paint();
	Bitmap b;
	Canvas c=new Canvas();
	Canvas textCanvas=new Canvas();
	int flakeLimit=10;
	int fps;
	long tStart=System.currentTimeMillis();
	int maxFPS = 20;
	int w,h;
	Bitmap textB;
	Context context;
	Thread t4;
	SharedPreferences prefs;
	SharedPreferences.Editor prefsEdit;
	float d;
	public SnowPlot(Context context, int w, int h)
	{
		d = context.getResources().getDisplayMetrics().density;

		prefs = PreferenceManager
			.getDefaultSharedPreferences(context);
		prefsEdit = prefs.edit();


		this.context = context;
		this.w = w;
		this.h = h;
		m = new int[w];


		//tfl.add(pl);
		LoadBottom();
		Thread t=new Thread(r1);
		t.setPriority(Thread.MIN_PRIORITY);
		t.start();
		tfl.add(pl);
		t4 = new Thread(new FlakesMover(pl, true));
		t4.setPriority(Thread.MIN_PRIORITY);
		t4.start();
		//new Thread(new r4(pl)).start();
		//t4.setPriority(Thread.MIN_PRIORITY);
		//t4.interrupt();
		//t4.start();

	}
	boolean colorful=false;
	int rf=1;
	int gf=2;
	int bf=4;

	public void setStepDuration(long stepDuration)
	{
		if (stepDuration > 33)
			this.stepDuration = stepDuration;
		else 
			this.stepDuration = 33;
	}

	public long getStepDuration()
	{
		return stepDuration;
	}

	public boolean touch(MotionEvent e)
	{
		switch (e.getActionMasked())
		{
			case MotionEvent.ACTION_UP:

			case MotionEvent.ACTION_DOWN:
			case MotionEvent.ACTION_POINTER_DOWN:
			case MotionEvent.ACTION_POINTER_UP:
				for (int size = e.getPointerCount(), i = 0; i < size; i++)
				{


					addFlakes(e.getX(i), e.getY(i), 100);


				}
				return true;
			case MotionEvent.ACTION_MOVE:
				for (int size = e.getPointerCount(), i = 0; i < size; i++)
				{
					addFlakes(e.getX(i), e.getY(i), 10);
				}
				return true;
		}
		return false;
	}
	public void setColorful(boolean colorful)
	{
		//red=255;rc=false;
		//green=255;gc=false;
		//blue=255;bc=false;
		rf = new Random().nextInt(10) + 1;
		gf = new Random().nextInt(10) + 1;
		bf = new Random().nextInt(10) + 1;

		this.colorful = colorful;
	}

	public void SaveBottom(int Offset)
	{

		// TODO Auto-generated method stub

		File myDir=context.getCacheDir();


		File file = new File(myDir, w + "_" + h + ".png");
		try
		{
			FileOutputStream out = new FileOutputStream(file);
			b.compress(Bitmap.CompressFormat.PNG, 100, out);

			out.flush();
			out.close();

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	public void LoadBottom()
	{
		File myDir=context.getCacheDir();


		File file = new File(myDir, w + "_" + h + ".png");
		if (file.exists())
		{

			try
			{
				FileInputStream in = new FileInputStream(file);
				b = BitmapFactory.decodeStream(in);
				b = b.copy(Bitmap.Config.ARGB_8888, true);
				c.setBitmap(b);
				//int max=0;
				int y=h / 2;
				for (int x=0;x < b.getWidth();x++)
				{
					if (y >= b.getHeight())y = b.getHeight() - 1;
					while (y < b.getHeight())
						if (b.getPixel(x, y) != Color.BLACK)
						{
							if (y > 0)
							{
								if (b.getPixel(x, y - 1) == Color.BLACK)
								{
									m[x] = h - y;
									//if (m[x] > max)max = m[x];
									break;
								}
							}
							else
							{resetSugrob();return;}
							y--;

						}
						else y++;
				}
				try
				{
					red = Color.red(b.getPixel(w / 2, h - m[w / 2]));
					green = Color.green(b.getPixel(w / 2, h - m[w / 2]));
					blue = Color.blue(b.getPixel(w / 2, h - m[w / 2]));
				}
				catch (Exception e)
				{}
				if (red + green + blue < 256)
					red = green = blue = 255;
				updateColor(1);
				/*
				 while (pl.size() < 100)
				 {
				 Flake f=new Flake();
				 f.x = new Random().nextInt(w);
				 f.y = new Random().nextInt(h);
				 f.setR((new Random().nextFloat() * 3) * d);
				 f.setColor(Color.rgb((int)red, (int)green, (int)blue));
				 pl.add(f);
				 currentflakes++;
				 }
				 */
				//if (max >= h)resetSugrob();
			}
			catch (Exception e)
			{
				b = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);      

				c.setBitmap(b);    
				c.drawColor(Color.BLACK);
			}


		}
		else
		{
			b = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);      

			c.setBitmap(b);    
			c.drawColor(Color.BLACK);
		}


	}
	public void setFrameDuration(long frameDuration)
	{
		this.frameDuration = frameDuration;
	}

	public long getFrameDuration()
	{
		return frameDuration;
	}

	public void snowText(String text)
	{
		drawText(text, 0, 300);
	}
	private float red=255;
	private float green=255;
	private float blue=	255;;
	private boolean rc;
	private boolean gc;
	private boolean bc;

	private void updateColor(int factor)
	{
		if (rc)
		{
			red += (new Random().nextFloat() * rf) / (200 * d) * factor;
			if (red > 255)
			{
				red = 255;rc = !rc;
				rf = new Random().nextInt(10) + 1;
			}
		}
		else
		{
			red -= (new Random().nextFloat() * rf) / (200 * d) * factor;
			if (red < 0)
			{
				red = 0;rc = !rc;
				rf = new Random().nextInt(10) + 1;

			}
		}
		if (gc)
		{
			green += (new Random().nextFloat() * gf) / (200 * d) * factor;
			if (green > 255)
			{
				green = 255;gc = !gc;
				gf = new Random().nextInt(10) + 1;

			}
		}
		else
		{
			green -= (new Random().nextFloat() * gf) / (200 * d) * factor;
			if (green < 0)
			{
				green = 0;gc = !gc;
				gf = new Random().nextInt(10) + 1;

			}
		}
		if (bc)
		{
			blue += (new Random().nextFloat() * bf) / (200 * d) * factor;
			if (blue > 255)
			{
				blue = 255;bc = !bc;
				bf = new Random().nextInt(10) + 1;

			}
		}
		else
		{
			blue -= (new Random().nextFloat() * bf) / (200 * d) * factor;
			if (blue < 0)
			{
				blue = 0;bc = !bc;
				bf = new Random().nextInt(10) + 1;

			}
		}
		if (colorful)
		{
			if (red + green + blue < 256)
			{
				if (!rc)rc = new Random().nextBoolean();
				if (!gc)gc = new Random().nextBoolean();
				if (!bc)gc = new Random().nextBoolean();
			}
		}
		else
		{
			if (red + green + blue < 600)
			{
				rc = gc = bc = true;
			}
		}
	}
	public void addFlakes(final float x, final float y, final int count)
	{


		List<Flake> pl = 
			Collections.synchronizedList(new ArrayList<Flake>());

		// TODO: Implement this method
		synchronized (pl)
		{
			updateColor(count);
			for (int i=0;i < count;i++)
			{
				Flake flake=new Flake();
				//int W=(int)-(w * Math.tan(angle / 180.0 * Math.PI));
				/*
				 if (W >= 0)
				 flake.x = new Random().nextInt(w + Math.abs(W));
				 else
				 flake.x = new Random().nextInt(w + Math.abs(W)) + W;
				 */

				int Size=random.nextInt(100) + 1;
				flake.x = x + random.nextInt(Size) - Size / 2;
				flake.y = y + random.nextInt(Size) - Size / 2;
				flake.setColor(getColor((int)flake.x));


				flake.setR((random.nextFloat() * 3) * d);
				//flake.setSpeed();
				//if (flake.x < w && flake.x > 0)
				pl.add(flake);

			}
		}
		try
		{
			new Thread(new FlakesMover(pl, false)).start();
			tfl.add(pl);
		}
		catch (Exception e)
		{}


	}

	public void movey()
	{
		synchronized (pl)
		{
			Iterator<Flake> it=pl.iterator();
			while (it.hasNext())
			{
				Flake f=it.next();
				f.y = (f.y + 150) % h;

			}

		}
		//catch (Exception e)
		{}
	}

	public synchronized void resetSugrob()
	{

		int minH=h;
		for (int H: m)
			if (H < minH)minH = H;
		for (int H: m)
			H -= minH;
		Bitmap tmpB = b.copy(b.getConfig(), true);
		c.drawColor(Color.BLACK);
		try
		{
			c.drawBitmap(tmpB, 0, minH, null);
		}
		catch (Exception e)
		{

		}
		m = new int[w];

		//b = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);      

		//c.setBitmap(b);    
		//c.drawColor(Color.BLACK);

		SaveBottom(0);
		Photo = null;

	}

	public void setShowTime(boolean showTime)
	{
		this.showTime = showTime;
	}

	public void move(final float x)
	{

		synchronized (pl)
		{
			ListIterator<Flake> it=pl.listIterator();
			while (it.hasNext())
			{
				//try
				{
					Flake f=it.next();
					//f.y+=y;
					f.x = ((f.x + (x * f.getR()) / 5) % w + w) % w;

				}
				//catch (ConcurrentModificationException e)
				{
					//break;
					//it.remove();
					//Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
					//return;
				}
			}
		}
	}

	public void setAngle(float angle)
	{
		this.angle = (angle / 180.0 * (float)Math.PI);
	}


	public void setFlakeLimit(int flakeLimit)
	{
		this.flakeLimit = flakeLimit;
	}


	long frameDuration=33;
	public void setMaxFPS(int maxFPS)
	{

		this.maxFPS = maxFPS;
		frameDuration = 1000 / maxFPS;
	}



	public void setW(int w)
	{
		this.w = w;
	}


	public void setH(int h)
	{
		this.h = h;
	}

	Random random=new Random();



	public void draw(Canvas canvas)
	{
		if (pl.size() < flakeLimit)
			try
			{
				t2 = new Thread(FlakesCreator);
				t2.start();
			}
			catch (InternalError e)
			{}

		canvas.drawBitmap(b, 0, 0, paint);

		//canvas.drawBitmap(clock, 0, 0, paint);

		if (maxFPS != -1)
		{
			long currentFD=System.currentTimeMillis() - tStart;
			if (currentFD < (frameDuration))
				flakeLimit ++;//= new Random().nextInt(1);
			else

				flakeLimit += ((frameDuration - currentFD) / 4 + 1);//(flakeLimit / 100 + 1);
			if (flakeLimit <= 0)
				flakeLimit = 0;
			tStart = System.currentTimeMillis();
		}



		//paint.setColor(Color.WHITE);
		/*
		 synchronized (pl)
		 {
		 Iterator<Flake> it=pl.iterator();
		 Flake f;
		 while (it.hasNext())
		 {
		 f = it.next();

		 //try
		 {
		 paint.setColor(f.getColor());
		 canvas.drawCircle(f.x, f.y, f.getR(), paint);
		 }
		 //catch (Exception e)
		 {
		 //continue;
		 }

		 }
		 }*/
		synchronized (tfl)
		{
			Iterator<List<Flake>> IT =tfl.iterator();
			while (IT.hasNext())
			{
				List<Flake> l=IT.next();
				synchronized (l)
				{
					Iterator<Flake> it = l.iterator();

					while (it.hasNext())
					{
						Flake f = it.next();

						//try
						{
							paint.setColor(f.getColor());
							canvas.drawCircle(f.x, f.y, f.getR(), paint);
						}
						//catch (Exception e)
						{
							//continue;
						}

					}
				}
			}
		}

		//super.onDraw(canvas);
	}

	private long stepDuration=33;
	private class FlakesMover implements Runnable
	{
		List<Flake> fl;
		boolean infloop;
		public FlakesMover(List<Flake> pl, boolean infloop)
		{
			this.fl = pl;
			this.infloop = infloop;
		}
		long t1=System.currentTimeMillis();


		@Override
		public void run()
		{

			//Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(context));

			while (infloop || fl.size() > 0)
			{
				try
				{
					long d=System.currentTimeMillis() - t1;
					if (d < stepDuration)
						Thread.sleep(stepDuration - d);

				}
				catch (InterruptedException e)
				{}
				//t4running = true;
				t1 = System.currentTimeMillis();

				synchronized (fl)
				{
					Iterator<Flake> it=fl.iterator();
					while (it.hasNext())
					{
						Flake f=it.next();


						int x=(random.nextInt(5) - 2);
						int y=0;


						y += (int)((random.nextInt(4)) + f.getSpeed());


						if (Photo == null)
						{
							f.x += (int)(x * Math.cos(angle) + y * Math.sin(angle));

							f.y += (int)((y) * Math.cos(angle) + x * Math.sin(angle));
						}
						else
						{
							f.x += x ;
							f.y += y;
						}
						f.x = m(f.x);



						int k=(int)f.x;


						if (f.y >= (h - m[k]))
						{
							int size=(int)f.getArea();

							for (int j=0;j < size ;j++)
							{
								addheight((int)f.x + random.nextInt(3) - 1, (int)f.y, f.getColor());
							}
							//addheight((int)f.x - random.nextInt(random.nextInt(w / 2) + 1) + random.nextInt(random.nextInt(w / 2) + 1), (int)f.y, f.getColor());

							//continue;





							it.remove();




							continue;

						}
						if (f.y < -h)
						{
							it.remove();

							continue;
						}

					}
				}
			}
			synchronized (tfl)
			{
				tfl.remove(fl);
			}

			//t4done = true;
			//super.onDraw(canvas);
		}
	};
	private int getColor(int x)
	{
		if (Photo == null)
		{
			updateColor(1);
			if (colorful)
				return Color.rgb((int)red, (int)green, (int)blue);
			else
			{
				int avg=(int)((red + green + blue) / 3.0f);

				return Color.rgb(avg, avg, avg);
			}

		}
		else
		{
			try
			{
				int y=h - m[x];
				y = y % Photo.getHeight();
				if (y < 0)
					y += Photo.getHeight();
				int c= Photo.getPixel(x, y);
				if (Color.red(c) + Color.green(c) + Color.blue(c) < 64)
					return Color.rgb(
						Color.red(c) + 64,
						Color.green(c) + 64,
						Color.blue(c) + 64
					);
				return c;
				//flake.setSpeed(5);
			}
			catch (Exception e)
			{
				return Color.GRAY;
			}
		}
	}
	Thread t2=new Thread();

	Runnable FlakesCreator=new Runnable(){

		@Override
		public void run()
		{

			//currentflakes=0;

			while (pl.size() < flakeLimit)
			{

				Flake flake=new Flake();

				flake.x = new Random().nextInt(w);

				flake.setR(new Random().nextFloat() * 3 * d);
				flake.y = -flake.getR();
				flake.setColor(getColor((int)flake.x));
				//flake.setSpeed(5);





				pl.add(flake);




			}

		}


	};
	boolean showTime=false;
	boolean goodMorningDone=false;
	Runnable r1=new Runnable(){

		@Override
		public void run()
		{

			Paint p=new Paint();
			p.setStrokeCap(Paint.Cap.ROUND);
			p.setStrokeWidth(100);
			p.setColor(Color.BLACK);
			int s=1000;
			long lasttime=System.currentTimeMillis() - 180000;
			while (true)
			{
				try
				{
					Thread.sleep(s);
				}
				catch (InterruptedException e)
				{}

				if (s == 1000 && showTime && pl.size() < flakeLimit * 2)
				{

					showTime = false;

					Calendar cal = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
					if (System.currentTimeMillis() - lasttime > 180000)
					{
						lasttime = System.currentTimeMillis();
						String time = sdf.format(cal.getTime());
						if (cal.get(Calendar.HOUR_OF_DAY) < 12 && cal.get(Calendar.HOUR) > 6)
						{
							if (!goodMorningDone)
							{
								drawText(context.getString(R.string.good), 1, 300);
								drawText(context.getString(R.string.morning), 2, 300);
							}
							else
								drawText(time, 1, 300);

							goodMorningDone = true;
						}
						else
						{
							goodMorningDone = false;
							drawText(time, 1, 300);
						}


					}
				}

				if (pl.size() > flakeLimit * 2) continue;

				if (angle > Math.PI / 2.0 && angle < 3.0 * Math.PI / 2.0)
				{
					s = 1;
					int x=new Random().nextInt(w);
					int S;
					if (random.nextBoolean())
						S = 1;
					else 
						S = -1;

					for (int k=x;k < m.length && k >= 0;k += S)
					{
						if (m[k] > 0)
						{
							Flake f=new Flake();
							if (m[k] < 100)
								f.setArea(m[k] * 10 + 1);
							else
								f.setArea(new Random().nextInt(m[k] + 1) * 10 + 1);
							f.x = k;
							f.y = h - m[k] - 10;
							//c.drawLine(k,  (h - m[k]) - 50, k, (h - m[k]) - 49, p);
							int c=0;
							int cc=0;
							long ar=0;
							long ag=0;
							long ab=0;
							for (int i = 0;i < f.getArea();i++)
							{
								c = removeheighti(k, h - m[k]);
								ar += Color.red(c);
								ag += Color.green(c);
								ab += Color.blue(c);
								cc++;
							}
							if (cc == 0)
								cc++;
							//m[k] -= (f.getArea()/10);
							c = Color.rgb((int)(ar / cc), (int)(ag / cc), (int)(ab / cc));
							f.setColor((c));
							pl.add(f);

							//c.drawColor(Color.BLACK);

							break;
						}
					}

				}
				else
					s = 1000;
			}


		}




	};
	Bitmap Photo=null;
	public void drawImage(Bitmap b, int offset)
	{
		Photo = Bitmap.createScaledBitmap(b, w, h, false);
		//clockCanvas.drawText(time, 0, 300, paint);
		//pl.clear();
		/*
		 synchronized (pl)
		 {
		 for (int i=0;i < flakeLimit * 2;i++)
		 {
		 int x=new Random().nextInt(B.getWidth());
		 int y=new Random().nextInt(B.getHeight());

		 if (B.getPixel(x, y) == Color.BLACK)continue;
		 Flake f=new Flake(x, y + offset);
		 f.setR(new Random().nextFloat() * 5 * d);
		 //f.setR(10);
		 f.setSpeed(5);

		 f.setColor(B.getPixel(x, y));
		 pl.add(f);
		 }
		 }
		 */
	}
	private void drawText(String Text, int offset, int textSize)
	{
		List<Flake> fl = 
			Collections.synchronizedList(new ArrayList<Flake>());

		Paint tpaint = new Paint();
		tpaint.setColor(Color.WHITE);
		tpaint.setTextSize(textSize);
		Rect bounds = new Rect();
		tpaint.getTextBounds(Text, 0, Text.length(), bounds);

		textB = Bitmap.createBitmap((int)tpaint.measureText(Text), textSize + 50, Bitmap.Config.ARGB_8888);      
		textCanvas.setBitmap(textB);
		int X = (textCanvas.getWidth() / 2) - ((int)tpaint.measureText(Text) / 2);

		textCanvas.drawColor(Color.BLACK);

		textCanvas.drawText(Text, X, textSize, tpaint);
		Bitmap copyb = Bitmap.createScaledBitmap(textB, w, (int)((float)textB.getHeight() / (float)textB.getWidth() * (float)w), false);
		//clockCanvas.drawText(time, 0, 300, paint);
		for (int y=copyb.getHeight() - 1;y >= 0 ;y -= new Random().nextInt(10))
		{
			for (int x=0;x < copyb.getWidth();x += new Random().nextInt(10))
			{

				if (copyb.getPixel(x, y) == Color.WHITE)
				{
					Flake f=new Flake(x, y + (copyb.getHeight() * offset) - copyb.getHeight());
					f.setR(new Random().nextFloat() * 3 * d);
					f.setSpeed(2);
					f.setColor(Color.rgb((int)red, (int)green, (int)blue));
					fl.add(f);


				}
			}
			updateColor(50);
		}
		Thread t5=new Thread(new FlakesMover(fl, false));


		while (true)
			try
			{
				if (!t5.isAlive())
					t5.start();
				break;
			}
			catch (IllegalThreadStateException e)
			{
				try
				{
					Thread.sleep(5000);
				}
				catch (InterruptedException e2)
				{}
				continue;
			}
		tfl.add(fl);
	}


	private boolean addheight(int x, int y, int c)
	{
		x = m(x);

		//% b.getWidth();
		//if (x < 0)x += b.getWidth();
		synchronized (b)
		{
			if (y >= (b.getHeight() - 1))
			{
				if (b.getPixel(x, b.getHeight() - 1) == Color.BLACK)
				{
					b.setPixel(x, b.getHeight() - 1, c);
					m[x] = 1;
					return true;
				}
				y = b.getHeight() - 2;
			}
			if (y <= 0)
				y = 0;
			//int cc;
			//synchronized(b){
			while ((b.getPixel(x, y)) != Color.BLACK)
			{
				y--;

				if (y < 0)
				{
					//y+=10;
					//cc=Color.BLACK;
					resetSugrob();
					return false;
					//return false;
				}
			}
			int s;
			if (random.nextBoolean())
				s = 2;
			else 	s = -2;
			/*
			 while (b.getPixel(m(x - 1), y + 1) == Color.BLACK)
			 {
			 x = m(x - 1);
			 //% w + w) % w;
			 y++;
			 if (y >= (b.getHeight() - 1))
			 {
			 if (b.getPixel(x, b.getHeight() - 1) == Color.BLACK)
			 {
			 synchronized (b)
			 {
			 b.setPixel(x, b.getHeight() - 1, c);
			 }
			 m[x] = 1;
			 return;
			 }
			 y = b.getHeight() - 2;
			 }
			 }*/
			for (int i=0;i < 2;i++)
			{
				while (b.getPixel(m(x + s) , y + 1) == Color.BLACK)
				{

					x = m(x + s);
					y++;
					//if(s<0)s--;
					//else s++;
					if (y >= (b.getHeight() - 1))
					{
						if (b.getPixel(x, b.getHeight() - 1) == Color.BLACK)
						{
							synchronized (b)
							{
								b.setPixel(x, b.getHeight() - 1, c);
							}
							m[x] = 1;
							return true;
						}
						y = b.getHeight() - 2;
					}
				}
				if (s > 0)	s = -2;
				else 		s = 2;
			}
			//if(random.nextBoolean())

			try
			{
				b.setPixel(x, y, c);
			}
			catch (Exception e)
			{}
		}
		m[x] = h - y;
		return true;

	}
	/*
	 private int addheightold(int x)
	 {
	 if (m[x] >= h){
	 SaveBottom(0);
	 return x;
	 }
	 if (x - 1 >= 0)
	 if (m[x] - m[x - 1] >= random.nextInt(2))
	 return addheight(x - 1);

	 if (x + 1 < w)
	 if (m[x] - m[x + 1] >= random.nextInt(2))
	 return addheight(x + 1);
	 m[x]++;
	 p.setStrokeWidth(100);
	 p.setStrokeCap(Paint.Cap.ROUND);
	 p.setColor(Color.WHITE);

	 c.drawLine(x,  (h - m[x]) + 51, x, (h - m[x]) + 50, p);

	 return x;
	 }*/


	private int removeheighti(int x, int y)
	{
		while (true)
		{
			x = m(x); 
			if (y >= h)
			{
				m[x] = 0;
				int c=b.getPixel(x, b.getHeight() - 1);
				b.setPixel(x, b.getHeight() - 1, Color.BLACK);
				return c;
			}
			if (b.getPixel(x, y) == Color.BLACK)
			{
				y++;
				continue;
				//return removeheight(x, y + 1);
			}
			int p=m(x - 1);
			//if (x - 1 >= 0)
			if (b.getPixel(p, y - 1) != Color.BLACK)
			{
				y--;
				x = p;
				continue;
				//return removeheight(p, y - 1);
			}

			int n=m(x + 1);
			if (b.getPixel(n, y - 1) != Color.BLACK)
			{
				x = n;
				y--;
				continue;
				//return removeheight(n, y - 1);
			}
			m[x] = h - y;
			int c=b.getPixel(x, y);
			b.setPixel(x, y, Color.BLACK);

			return c;

		}

	}
	/*
	 private int removeheight(int x, int y)
	 {
	 x = x % b.getWidth();
	 if (x < 0) x += b.getWidth();
	 if (y >= h)
	 {
	 m[x] = 0;
	 return Color.WHITE;
	 }
	 if (b.getPixel(x, y) == Color.BLACK)
	 return removeheight(x, y + 1);

	 //x = (x - 1) % b.getWidth();
	 //if (x < 0)x += b.getWidth();
	 //y--;
	 //if (x - 1 >= 0)
	 //      
	 //     # 
	 //    ###
	 //   #####          #
	 //  #######        ###
	 // #########      #####
	 //######################
	 if (b.getPixel(m(x - 1), y - 1) != Color.BLACK)
	 {
	 while (b.getPixel(m(--x), --y) != Color.BLACK) continue;


	 x = m(x + 1);y++;
	 while (b.getPixel(x, y) == Color.BLACK)
	 {
	 y++;
	 if (y >= b.getHeight())
	 {
	 m[x] = 0;
	 b.setPixel(x, b.getHeight() - 1, Color.BLACK);
	 return Color.WHITE;
	 }
	 }
	 }
	 else
	 {
	 //x = (x + 1) % b.getWidth();
	 //y++;
	 while (b.getPixel(m(++x), --y) != Color.BLACK) continue;

	 x = m(x - 1);y++;
	 while (b.getPixel(x, y) == Color.BLACK)
	 {
	 y++;
	 if (y >= b.getHeight())
	 {
	 m[x] = 0;
	 b.setPixel(x, b.getHeight() - 1, Color.BLACK);
	 return Color.WHITE;
	 }
	 }
	 }
	 //y++;

	 //x=(x + 1) % b.getWidth();
	 //if (x + 1 < w)

	 m[x]--;
	 int c=b.getPixel(x, y);
	 b.setPixel(x, y, Color.BLACK);
	 return c;
	 }
	 */
	float m(float n)
	{
		n = n % b.getWidth();
		if (n < 0)n += b.getWidth();
		return n;
	}
	int m(int n)
	{
		n = n % b.getWidth();
		if (n < 0)n += b.getWidth();
		return n;
	}
}
