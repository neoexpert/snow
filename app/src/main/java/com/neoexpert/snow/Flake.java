package com.neoexpert.snow;
import android.graphics.*;

public class Flake extends PointF
{
	private int color=Color.WHITE;
	public Flake(float x, float y){
		super(x,y);
		
	}
	public Flake(){
		super();
	}
	float speed=0;
	float radius;

	public void setColor(int color)
	{
		if(color==0)color=Color.WHITE;
		this.color = color;
	}

	public int getColor()
	{
		return color;
	}

	public void setSpeed(float s)
	{
		speed=s;
		// TODO: Implement this method
	}
	public void setR(float r)
	{
		A = (float)((Math.PI * Math.pow(r, 2))/10.0);
		this.radius = r;
		speed=r;
	}
	private float A;
	public void setArea(float A)
	{
		this.A = A/10.0f;
		radius = (float)Math.sqrt(A / Math.PI);
		speed=radius;
	}
	public float getArea()
	{
		return A;
	}
	float getSpeed()
	{
		return speed;
	}
	public float getR()
	{
		return radius;
	}
}
