package com.neoexpert.snow;

import android.app.*;
import android.os.*;
import android.preference.*;
import android.text.*;
import android.view.*;
import android.widget.*;
import com.neoexpert.snow.*;
import android.content.*;
import android.widget.RadioGroup.*;

public class WallpaperSettings extends Activity
implements
CompoundButton.OnCheckedChangeListener
{

	@Override
	public void onCheckedChanged(CompoundButton p1, boolean checked)
	{
		switch (p1.getId())
		{
			case R.id.sSnowTime:
				prefsEdit.putBoolean("Wallpaper_SnowTime", checked);

				break;
			case R.id.sTouchAnimation:
				prefsEdit.putBoolean("Wallpaper_TouchAnimation", checked);

				break;
			case R.id.sColorful:
				prefsEdit.putBoolean("Wallpaper_Colorful", checked);

				break;
		}
		prefsEdit.commit();
	}


	TextWatcher t1=new TextWatcher(){

		@Override
		public void beforeTextChanged(CharSequence p1, int p2, int p3, int p4)
		{
			// TODO: Implement this method
		}

		@Override
		public void onTextChanged(CharSequence p1, int p2, int p3, int p4)
		{
			// TODO: Implement this method
		}

		@Override
		public void afterTextChanged(Editable p1)
		{
			try
			{
				if (new Integer(p1.toString()) <= 0)
					throw new NumberFormatException();
				prefsEdit.putInt("Wallpaper_MaxFlakes", new Integer(p1.toString()));
				prefsEdit.commit();
			}
			catch (NumberFormatException e)
			{}
			// TODO: Implement this method
		}
	};
	TextWatcher t2=new TextWatcher(){

		@Override
		public void beforeTextChanged(CharSequence p1, int p2, int p3, int p4)
		{
			// TODO: Implement this method
		}

		@Override
		public void onTextChanged(CharSequence p1, int p2, int p3, int p4)
		{
			// TODO: Implement this method
		}

		@Override
		public void afterTextChanged(Editable p1)
		{
			try
			{
				if (new Integer(p1.toString()) <= 0)
					throw new NumberFormatException();
				prefsEdit.putInt("Wallpaper_Sleeptime", new Integer(p1.toString()));
				prefsEdit.commit();
			}
			catch (NumberFormatException e)
			{}
			// TODO: Implement this method
		}
	};
	SharedPreferences prefs;
	SharedPreferences.Editor prefsEdit;
	protected void onCreate(Bundle savedInstanceState)
    {
		//Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.wallpaper_settings);

		prefs = PreferenceManager
			.getDefaultSharedPreferences(this);
		prefsEdit = prefs.edit();
		EditText etMaxFlakes=(EditText)findViewById(R.id.sMaxFlakes);

		etMaxFlakes.addTextChangedListener(t1);


		etMaxFlakes.setText("" + prefs.getInt("Wallpaper_MaxFlakes", 100));
		EditText etSleepTime=(EditText)findViewById(R.id.sSleepTime);
		etSleepTime.addTextChangedListener(t2);
		etSleepTime.setText("" + prefs.getInt("Wallpaper_Sleeptime", 50));
		CheckBox cbSnowTime=(CheckBox)findViewById(R.id.sSnowTime);
		cbSnowTime.setOnCheckedChangeListener(this);
		cbSnowTime.setChecked(prefs.getBoolean("Wallpaper_SnowTime", true));
		CheckBox cbTouchAnimation=(CheckBox)findViewById(R.id.sTouchAnimation);
		cbTouchAnimation.setOnCheckedChangeListener(this);
		cbTouchAnimation.setChecked(prefs.getBoolean("Wallpaper_TouchAnimation", true));
		CheckBox cbColorful=(CheckBox)findViewById(R.id.sColorful);
		cbColorful.setOnCheckedChangeListener(this);
		cbColorful.setChecked(prefs.getBoolean("Wallpaper_Colorful", true));

	}
}
