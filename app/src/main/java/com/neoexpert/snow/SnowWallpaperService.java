package com.neoexpert.snow;
import android.content.*;
import android.graphics.*;
import android.os.*;
import android.preference.*;
import android.service.wallpaper.*;
import android.view.*;
import java.util.*;

public class SnowWallpaperService extends WallpaperService
{
	SharedPreferences prefs;
	SharedPreferences.Editor prefsEdit;
	@Override
	public Engine onCreateEngine()
	{
		if(BuildConfig.DEBUG)
			Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
		
		prefs = PreferenceManager
			.getDefaultSharedPreferences(SnowWallpaperService.this);
		prefsEdit = prefs.edit();
		
	 	return new MyWallpaperEngine();
	}
	
	
	private int width;
	private int height;
	SnowPlot sp=null;
	private class MyWallpaperEngine extends Engine
	implements
    SharedPreferences.OnSharedPreferenceChangeListener 
	{

		@Override
		public void onSharedPreferenceChanged(SharedPreferences p1, String p2)
		{
			switch(p2){
				case "Wallpaper_Sleeptime":
					sleep=prefs.getInt("Wallpaper_Sleeptime",50);
					sp.setFrameDuration(sleep);
					sp.setStepDuration(sleep);
					
				break;
				case "Wallpaper_MaxFlakes":
					sp.setFlakeLimit(prefs.getInt("Wallpaper_MaxFlakes",100));
					break;
				case "Wallpaper_TouchAnimation":
					touchAnimation=prefs.getBoolean("Wallpaper_TouchAnimation",true);
				break;
				case "Wallpaper_Colorful":
					sp.setColorful(prefs.getBoolean("Wallpaper_Colorful",true));
					break;
					
			}
		}
		
		private final Handler handler = new Handler();
		private final Runnable drawRunner = new Runnable() {
			@Override
			public void run()
			{
				if (!touch)
					draw();
			}

		};

		private Paint paint = new Paint();
		public MyWallpaperEngine()
		{
			paint.setColor(Color.WHITE);
			paint.setStyle(Paint.Style.STROKE);
			paint.setStrokeJoin(Paint.Join.ROUND);
			paint.setStrokeWidth(10f);
			handler.post(drawRunner);
			sleep=prefs.getInt("Wallpaper_Sleeptime",50);
			touchAnimation=prefs.getBoolean("Wallpaper_TouchAnimation",true);
			prefs.registerOnSharedPreferenceChangeListener(this);
		}
		private boolean visible = true;

		//private boolean touchEnabled;
		@Override
		public void onVisibilityChanged(boolean visible)
		{

			
			this.visible = visible;
			if (visible)
			{
				
				
		
				
				sp.setShowTime(prefs.getBoolean("Wallpaper_SnowTime",true));
				handler.post(drawRunner);
			}
			else
			{
				sp.movey();
				SurfaceHolder holder = getSurfaceHolder();
				Canvas canvas = null;
				try
				{
					canvas = holder.lockCanvas();
					if (canvas != null)
					{
						canvas.drawColor(Color.BLACK);

						sp.draw(canvas);


					}
				}
				finally
				{
					if (canvas != null)
						holder.unlockCanvasAndPost(canvas);
				}
				//SaveBottom(0);
				handler.removeCallbacks(drawRunner);
			}
		}


		@Override
		public void onSurfaceDestroyed(SurfaceHolder holder)
		{
			//SaveBottom(0);
			sp.SaveBottom(0);
			super.onSurfaceDestroyed(holder);
			this.visible = false;
			handler.removeCallbacks(drawRunner);
		}

		@Override
		public void onSurfaceChanged(SurfaceHolder holder, int format,
									 int w, int h)
		{
			//SaveBottom(0);
			if(sp!=null)
				sp.SaveBottom(0);
			width = w;
			height = h;
			sp = new SnowPlot(getApplicationContext(),w, h);
			sp.setMaxFPS(-1);
			
			sp.setFrameDuration(prefs.getInt("Wallpaper_Sleeptime",100));
			sp.setStepDuration(sp.getFrameDuration());
			sp.setFlakeLimit(prefs.getInt("Wallpaper_MaxFlakes",100));
			sp.setColorful(prefs.getBoolean("Wallpaper_Colorful",true));
			sp.LoadBottom();
			//LoadBottom();
			
			super.onSurfaceChanged(holder, format, width, height);
		}
		float oldx=-1;
		float oldy=-1;
		boolean touch;
		boolean touchAnimation=true;
		@Override
		public void onTouchEvent(MotionEvent event)
		{
			switch (event.getAction())
			{
				case MotionEvent.ACTION_MOVE:
					touch = true;
					float x = event.getX();
					float y = event.getY();
					if (oldx == -1)oldx = x;
					if (oldy == -1)oldy = y;
					sp.move((oldx - x) / 2);
					oldx = x;
					oldy=y;
					SurfaceHolder holder = getSurfaceHolder();
					Canvas canvas = null;
					try
					{
						canvas = holder.lockCanvas();
						if (canvas != null)
						{
							canvas.drawColor(Color.BLACK);
							if(touchAnimation)
								sp.touch(event);
								//sp.addFlakes(x,y,4);
							sp.draw(canvas);


						}
					}
					finally
					{
						if (canvas != null)
							holder.unlockCanvasAndPost(canvas);
					}
					super.onTouchEvent(event);
					touch = false;
					break;
				case MotionEvent.ACTION_UP:
					oldx = -1;
					oldy=-1;
					break;
			}

		}
		private int sleep=50;
		private void draw()
		{
			SurfaceHolder holder = getSurfaceHolder();
			Canvas canvas = null;
			try
			{
				canvas = holder.lockCanvas();
				if (canvas != null)
				{

					canvas.drawColor(Color.BLACK);
					//drawCircles(canvas, circles);
					sp.draw(canvas);
				}
			}
			finally
			{
				if (canvas != null)
					holder.unlockCanvasAndPost(canvas);
			}
			handler.removeCallbacks(drawRunner);
			if (visible)
			{
				if(sleep<1)
					sleep=1;
				handler.postDelayed(drawRunner, sleep);
			}
		}


	} 

}
