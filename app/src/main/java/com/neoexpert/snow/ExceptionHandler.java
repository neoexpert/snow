package com.neoexpert.snow;

import java.io.PrintWriter;
import java.io.StringWriter;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.widget.*;
import android.content.*;
import java.io.*;
import android.os.*;

public class ExceptionHandler implements
java.lang.Thread.UncaughtExceptionHandler {
    private final Context myContext;
    private final String LINE_SEPARATOR = "\n";

    public ExceptionHandler(Context context) {
        myContext = context;
    }

    public void uncaughtException(Thread thread, Throwable exception) {
        StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        StringBuilder errorReport = new StringBuilder();
        errorReport.append("************ CAUSE OF ERROR ************\n\n");
        errorReport.append(stackTrace.toString());

        errorReport.append("\n************ DEVICE INFORMATION ***********\n");
        errorReport.append("Brand: ");
        errorReport.append(Build.BRAND);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Device: ");
        errorReport.append(Build.DEVICE);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Model: ");
        errorReport.append(Build.MODEL);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Id: ");
        errorReport.append(Build.ID);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Product: ");
        errorReport.append(Build.PRODUCT);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("\n************ FIRMWARE ************\n");
        errorReport.append("SDK: ");
        errorReport.append(Build.VERSION.SDK);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Release: ");
        errorReport.append(Build.VERSION.RELEASE);
        errorReport.append(LINE_SEPARATOR);
        errorReport.append("Incremental: ");
        errorReport.append(Build.VERSION.INCREMENTAL);
        errorReport.append(LINE_SEPARATOR);
		String p=Environment.getExternalStorageDirectory()+
			"/Snow/";
		File file =new File(p);
		file.mkdir();
		file = new File(p+"crashlog.txt");
		//Toast.makeText(getApplicationContext(),file.toString(),Toast.LENGTH_LONG).show();
		try
		{
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
			out.print(errorReport.toString());
			out.close();
		}
		catch (IOException e)
		{}//   Intent intent = new Intent(myContext, ForceClose.class);
		//   intent.putExtra("error", errorReport.toString());
		//  myContext.startActivity(intent);

		// android.os.Process.killProcess(android.os.Process.myPid());
        //System.exit(10);

    }

}
