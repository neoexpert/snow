package com.neoexpert.snow;

import android.content.*;
import android.graphics.*;
import android.hardware.*;
import android.preference.*;
import android.util.*;
import android.view.*;

public class SnowView extends SurfaceView
implements Runnable
{

	@Override
	public void run()
	{
		while(isItOk){

			if(!holder.getSurface().isValid()){
				continue;
			}

				Canvas c = holder.lockCanvas();
			c.drawARGB(255, 0, 0, 0);
			onDraw(c);
			holder.unlockCanvasAndPost(c);
		}
	}
	Thread t = null;
	SurfaceHolder holder;
	boolean isItOk = false ;

	

	float l=0;
	float oldx=-1,oldy=-1;
	
	
	
	Context context;
	public SnowView(Context context)
	{
		super(context);
		this.context = context;
		
		// TODO Auto-generated constructor stub
		holder = getHolder();
		init();
		
	}
	int angle;
	

	public SnowView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
		init();
	}

	public SnowView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		this.context = context;
		init();
	}
	
	public void pause(){
		/*
		isItOk = false;
		while(true){
			try{
				t.join();
			}catch(InterruptedException e){
				e.printStackTrace();
			}
			break;
		}
		t = null;
		*/
	}

	public void resume(){
		/*
		isItOk = true;  
		t = new Thread(this);
		t.start();
		*/

	}
	
	private void init()
	{
		OrientationEventListener myOrientationEventListener
			= new OrientationEventListener(context, SensorManager.SENSOR_DELAY_NORMAL){

			@Override
			public void onOrientationChanged(int arg0)
			{
				if (arg0 != 90 && arg0 != 270)
					sp.setAngle(arg0);
				//Toast.makeText(context, ""+arg0, Toast.LENGTH_LONG).show();

			}};

		if (myOrientationEventListener.canDetectOrientation())
		{
			//Toast.makeText(this, "Can DetectOrientation", Toast.LENGTH_LONG).show();
			myOrientationEventListener.enable();
		}
		else
		{
			//finish();
		}
	}






	SnowPlot sp;

	public void setSp(SnowPlot sp)
	{
		this.sp = sp;
	}


    @Override
	protected void onDraw(Canvas canvas)
	{
		//canvas.save();
		sp.draw(canvas);
		//canvas.restore();
		postInvalidate();
		//invalidate();
	}




}
